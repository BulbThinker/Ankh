#ifndef ANIMATION_HPP_INCLUDED
#define ANIMATION_HPP_INCLUDED

#include <memory>
#include <vector>

#include "Entity.hpp"

class Character;

struct CarryAnimationState{
    Character* const source;
    int steps;

    virtual bool activate();

    CarryAnimationState(Character* _source, int _steps):
        source(_source), steps(_steps)
    {}
};

struct CarryMovementState: public CarryAnimationState {
    const Direction direction;

    virtual bool activate();

    CarryMovementState(Character* _source, int _steps, Direction _direction):
        CarryAnimationState(_source, _steps),
        direction(_direction)
    {}
};

extern std::vector<unique_ptr<CarryAnimationState>> CASv;

#endif
