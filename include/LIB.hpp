/**============================================================================
 *  Project Name    <Ankh>
 *  Version         <1.0>
 *  Authors         <Matheus Dias>
 *  File Name       <LIB.hpp>
/=============================================================================*/
#ifndef LIB_HPP_INCLUDED
#define LIB_HPP_INCLUDED
/**=========================================================================**/
#include <vector>
#include <allegro5/allegro.h>
#include "../include/GUI.hpp"
/**=========================================================================**/
using namespace std;
/**=========================================================================**/
template<class _TP>
class LIB{
    protected:
        vector<_TP> data;
    public:
        LIB(){}
        virtual ~LIB(){}
        virtual _TP operator[](unsigned int i);
        virtual void add(_TP element);
        unsigned int size(void);
        virtual void free(void)=0;
};
/**=========================================================================**/
class LIB_BITMAP : public LIB<ALLEGRO_BITMAP*>{
    public:
        LIB_BITMAP():LIB<ALLEGRO_BITMAP*>(){}
        ~LIB_BITMAP();
        void free(void);
};
/**=========================================================================**/
class LIB_GUI : public LIB<GUI*>{
    public:
        LIB_GUI():LIB<GUI*>(){}

        ~LIB_GUI();
        void free(void);
};
/**=========================================================================**/
class LIB_SFX : public LIB<ALLEGRO_SAMPLE*>{
    public:
        LIB_SFX():LIB<ALLEGRO_SAMPLE*>(){}
        ~LIB_SFX();
        void free(void);
};
/**=========================================================================**/
#endif // LIB_HPP_INCLUDED
