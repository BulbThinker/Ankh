/**==================================================================
//  Project: <Ankh>
//  Version: <1.0>
//  Author:  <Matheus Dias>
//  File:    <config.hpp>
//================================================================**/
#ifndef CONFIG_HPP_INCLUDED
#define CONFIG_HPP_INCLUDED
/**===============================================================**/
#include <allegro5/allegro.h>
/**===============================================================**/
#define DEFAULT_FPS 60
#define DEFAULT_SCREEN_W 640
#define DEFAULT_SCREEN_H 480
#define DEFAULT_GAIN    0.75

#define CONFIG_F_PATH    "ankh_config.cfg"
#define ANKH_APP_NAME    "ANKH"
#define ANKH_APP_ICON    "resources/Ankh.png"
#define ANKH_TU_S        40
#define MUSIC_PATH       "resources/audio/soundtrack.wav"
const int ENEMYNUMBER = 20;

/**===============================================================**/
class CONFIG {
    public:
        ///SCREEN
        static ALLEGRO_CONFIG* cfg;
        static unsigned int fps;
        static unsigned int screen_w;
        static unsigned int screen_h;
        ///AUDIO
        static double gain;
        static double sfx_gain;
        ///FUNCOES
        static void saveFile(const char* path);
        static bool openFile(const char* path);
        static void loadConf();
        static void creatConf();
        static void destrConf();
};
/**===============================================================**/
#endif // CONFIG_HPP_INCLUDED
