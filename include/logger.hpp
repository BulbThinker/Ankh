/**============================================================================
 *  Project Name    <Ankh>
 *  Version         <1.0>
 *  Authors         <Ricardo Ardissone>
 *  File Name       <logger.hpp>
/=============================================================================*/
#ifndef LOGGER_HPP_INCLUDED
#define LOGGER_HPP_INCLUDED

#define LOG_FILE_PATH "log.txt"
/**==========================================================================**/
#include <ctime>
#include <iostream>
#include <fstream>
#include <stdlib.h>
/**==========================================================================**/
using namespace std;

// Singleton
class Logger {
    ofstream log_file;
    Logger();
    ~Logger();
    void open_log_file(const char* path);
    void close_log_file();
    void printLog(const string mensage);
    public:
        static Logger& getI();
        void criticalError(const string mensage);
        void operator <<(const string mensage);
};

/**==========================================================================**/
#endif // LOGGER_HPP_INCLUDED
