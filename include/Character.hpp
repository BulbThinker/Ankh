/**============================================================================
 *  Project Name	<Ankh>
 *  Version			<1.0>
 *  Authors		    <Natan Junges>
 *  File Name		<Character.hpp>
/=============================================================================*/
#ifndef CHARACTER_HPP_INCLUDED
#define CHARACTER_HPP_INCLUDED
/**==========================================================================**/
#include <vector>
#include <memory>
#include <functional>

#include <sqlite3.h>
#include <allegro5/allegro.h>

#include "../include/Entity.hpp"
#include "../include/SFX_PLAYER.hpp"
#include "../include/World.hpp"
#include "../include/Stat.hpp"
#include "Buff.hpp"
/**==========================================================================**/

class Buff;
class TemporaryBuff;
class Ability;

class Character : public Entity {
    Direction direction;

    public:
    Character(string _image, int _xPos, int _zPos, int _hp, int _mp, int _ap, int _maxHp, int _maxMp, int _maxAp);

    Stat<int> hp;
    Stat<int> mp;
    Stat<int> ap;

    std::vector<unique_ptr<Ability>> abilities;
    std::vector<unique_ptr<Buff>> buffs;

    int char_type;

    unique_ptr<Buff> stats_buff(char const * const name, int hpDiff, int mpDiff, int apDiff, bool show, bool use_color=false, ALLEGRO_COLOR color=al_map_rgba(255,255,255,255), ALLEGRO_BITMAP* bitmap=nullptr);
    unique_ptr<Buff> temporary_stats_buff(char const * const name, int hpDiff, int mpDiff, int apDiff, int end_turn, bool show, bool use_color=false, ALLEGRO_COLOR color=al_map_rgba(255,255,255,255));

    unique_ptr<Ability> end_turn_ability(char const * const name, int keycode, bool show = true);
    unique_ptr<Ability> friendly_stats_ability(char const * const name, int hp, int mp, int ap, int keycode, bool show = true, SFXs sfxs = SFXs::NOP);
    unique_ptr<Ability> friendly_stats_ability(char const * const name, int hp, int mp, int ap, int keycode, EntityStateAction esa, bool show = true, SFXs sfxs = SFXs::NOP);
    unique_ptr<Ability> teleport_ability(char const * const name, int hp, int mp, int ap, int keycode, SFXs sfsx = SFXs::NOP);
    unique_ptr<Ability> move_ability(char const * const name, int hp, int mp, int ap, int keycode, Direction direction);

    unique_ptr<Ability> target_stats_ability
    (
     char const * const name, int hpDiff, int mpDiff, int apDiff, int keycode, int thpDiff, int tmpDiff, int tapDiff, int range, bool show = true, SFXs sfxs = SFXs::NOP, EntityStateAction esa=EntityStateAction::casting);
    unique_ptr<Ability> target_stats_ability
    (
     char const * const name, int hpDiff, int mpDiff, int apDiff, int keycode, int thpDiff, int tmpDiff, int tapDiff, int range, std::function<std::unique_ptr<Buff>(Character&)> tb, bool show = true, SFXs sfxs = SFXs::NOP, EntityStateAction esa=EntityStateAction::casting);

    bool has_buff(const std::string & buff_name);

    virtual void drawSprite(coord origin, ALLEGRO_COLOR color=al_map_rgba(255,255,255,255));

    unique_ptr<Ability>* activate(int keycode);

    void end_turn(int turn, World& world);

    void activateAnimation(EntityStateAction esa);

    void storegame(sqlite3* db, std::string& savename);
    void loadgame(sqlite3* db, std::string& savename);
};


class PlayerCharacter: public Character {
    public:

    PlayerCharacter(string _image, int _xPos, int _zPos, int _hp, int _mp, int _ap, int _maxHp, int _maxMp, int _maxAp);

    static PlayerCharacter* def_pc(World& world, string image="character", Coord coord=Coord{0,0}, int hp=10, int mp=10, int ap=5, int maxHp=10, int maxMp=10, int maxAp=5);

};

template <typename AIT> class AICharacter: public Character {
    public:

    AICharacter(string _image, int _xPos, int _zPos, int _hp, int _mp, int _ap, int _maxHp, int _maxMp, int _maxAp);

    AIT ai;

    static AICharacter* random_monster(World& world, string image, int hp=10, int mp=10, int ap=5, int maxHp=10, int maxMp=10, int maxAp=10);
    static AICharacter* random_monster(World& world, string image, Coord coord, int hp=10, int mp=10, int ap=5, int maxHp=10, int maxMp=10, int maxAp=5);
    static AICharacter* random_monster(World& world, int hp=6, int mp=5, int ap=5, int maxHp=6, int maxMp=5, int maxAp=5);
    static AICharacter* level_monster(World& world, int level);

    static AICharacter* shadow_monster(World& world, int hp=10, int mp=10, int ap=5, int maxHp=10, int maxMp=10, int maxAp=10);
    static AICharacter* shadow_monster(World& world, Coord coord, int hp=10, int mp=10, int ap=5, int maxHp=10, int maxMp=10, int maxAp=5);
};

/**==========================================================================**/
#endif //CHARACTER_HPP_INCLUDED
