#ifndef ABILITY_HPP_INCLUDED
#define ABILITY_HPP_INCLUDED

#include <memory>

#include <allegro5/allegro.h>

#include "Character.hpp"
#include "World.hpp"
#include "Entity.hpp"
#include "EADisplay.hpp"
#include "Effect.hpp"

#include "AI.hpp"
#include "../include/SFX_PLAYER.hpp"
#include "EffectGroup.hpp"
#include "Animation.hpp"

class Character;

struct PlaySFX {
    const SFXs sfxs;

    PlaySFX(SFXs _sfxs):
        sfxs(_sfxs)
        {}
};

class Ability: public EADisplay, public PlaySFX {
    SourceEffectGroup effects;
    std::vector<TargetEffectGroup> targetEffects;

    public:
    Ability(Character* _source, string _name, bool _show, int _keycode, std::vector<unique_ptr<Effect>> _effects, SFXs _sfxs=SFXs::NOP, std::vector<TargetEffectGroup> _targetEffects = std::vector<TargetEffectGroup>()):
        EADisplay(_name, _show),
        PlaySFX(_sfxs),
        effects(_source, std::move(_effects)),
        targetEffects(std::move(_targetEffects)),
        selected(false),
        keycode(_keycode)
        {}
    virtual ~Ability(){}


    bool selected;
    bool active(
        PlayerCharacter& per,
        // const std::vector<AICharacter<RandomAI>*>& friendlies,
        const std::vector<AICharacter<RandomAI>*>& hostiles,
        const World& world
        );
    const int keycode;

    void activate(World& world, PlayerCharacter& per, std::vector<AICharacter<RandomAI>*> hostiles);
    bool validTarget(const Character& tc, const World& world) const;
    void insertTarget(Character * const tc, const World& world);
    void reset();

    bool calculateTargets(std::vector<Character*>& targets,
                          PlayerCharacter& per,
//                          const std::vector<AICharacter<RandomAI>*>& friendlies,
                          const std::vector<AICharacter<RandomAI>*>& hostiles,
                          const World& world);

    virtual void drawEA(ALLEGRO_FONT* ability_font, int posX, int posY, PlayerCharacter& per, std::vector<AICharacter<RandomAI>*> hostiles, const World& world);

    friend Character;
};

#endif //ABILITY_HPP_INCLUDED
