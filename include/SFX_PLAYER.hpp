/**============================================================================
 *  Project Name    <Ankh>
 *  Version         <1.0>
 *  Authors         <Matheus Dias>
 *  File Name       <SFX_PLAYER.hpp>
/=============================================================================*/
#include "../include/LIB.hpp"
#include <allegro5/allegro.h>
/**=========================================================================**/
#ifndef SFX_PLAYER_HPP_INCLUDED
#define SFX_PLAYER_HPP_INCLUDED
/**=========================================================================**/
enum class SFXs {NOP=0,PUNCH_SFX,STEP_SFX,TELEPORT_SFX,FIREBALL_SFX};
/**=========================================================================**/
class SFX_PLAYER{
    public:
        static LIB_SFX sfx;
        static void play(SFXs s);
        static void stop(void);
        static void free(void);
        static void load(void);
};
/**=========================================================================**/
#endif // SFX_PLAYER_HPP_INCLUDED
