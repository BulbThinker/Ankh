#ifndef EADISPLAY_HPP_INCLUDED
#define EADISPLAY_HPP_INCLUDED

#include <string>

#include "allegro.h"
#include "util.hpp"

struct EADisplay {
    const std::string name;
    const bool show;
    EADisplay(std::string _name, bool _show): name(_name), show(_show) {}
    virtual ~EADisplay(){}
    virtual void drawEA(ALLEGRO_FONT* ability_font, int posX, int posY);
};

#endif // EADISPLAY_HPP_INCLUDED
