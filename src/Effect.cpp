#include "../include/Effect.hpp"
#include "../include/util.hpp"
#include "../include/logger.hpp"
#include "../include/config.hpp"
#include "../include/World.hpp"

EffectStatus EffectT::valid_target(const Character&, const Character& target, const World& world){
    return effect->valid_target(target, world);
}
void EffectT::activate(Character&, Character& target, World& world){
    effect->activate(target, world);
}
EffectStatus EffectS::valid_target(const Character& source, const Character&, const World& world){
    return effect->valid_target(source, world);
}
void EffectS::activate(Character& source, Character&, World& world){
    effect->activate(source, world);
}

EffectStatus RotateToTarget::valid_target(const Character&, const Character&, const World&){
    return EffectStatus::pointless;
}

EffectStatus AlwaysPointless::valid_target(const Character&, const World&){
    return EffectStatus::pointless;
}

void RotateToTarget::activate(Character& source, Character& target, World&){
    source.entitystatedirection = dir2esa(source.closestDirection(target));
}

void RandomTeleport::activate(Character& source, World& world){
    source.setPosition(world.random_position());
}

void Move::activate(Character& source, World&){
    source.entitystateaction = EntityStateAction::walk;
    CASv.push_back(unique_ptr<CarryAnimationState>(new CarryMovementState(&source, Entity::getTileSize(), direction)));
}

EffectStatus Move::valid_target(const Character& target, const World& world){
    bool canmove;
    switch(direction){
    case _up:
        canmove = target.canMove(0, -world.getTileSize());
        break;
    case _down:
        canmove = target.canMove(0, world.getTileSize());
        break;
    case _left:
        canmove = target.canMove(-world.getTileSize(), 0);
        break;
    case _right:
        canmove = target.canMove(world.getTileSize(), 0);
        break;
    case _stop:
    default:
        Logger::getI().criticalError("Move::valid_target: invalid direction");
        canmove = false;
    }

    if(canmove){
        return EffectStatus::useful;
    } else {
        return EffectStatus::impossible;
    }
}

void EndTurn::activate(Character& source, World&){
    source.ap = 0;
}

void Animate::activate(Character& source, World&){
    source.activateAnimation(animation_action);
}

void AnimateIfDead::activate(Character& source, World& world){
    if(source.hp <= 0){
        Animate::activate(source, world);
    }
}

void StatsEffect::activate(Character& source, World&){
    source.hp += hp_effect;
    source.mp += mp_effect;
    source.ap += ap_effect;
}

EffectStatus AlwaysValidTarget::valid_target(const Character&, const World&){
    return EffectStatus::useful;
}

EffectStatus EndTurn::valid_target(const Character& target, const World&){
    if(target.ap > 0){
        return EffectStatus::useful;
    } else {
        return EffectStatus::impossible;
    }
}

EffectStatus FriendlyStatsEffect::valid_target(const Character& target, const World&){
    if (target.hp + hp_effect < 0 ||
        target.mp + mp_effect < 0 ||
        target.ap + ap_effect < 0) {
        return EffectStatus::impossible; // Not enough hp, mp or ap to do it
    }

    if(hp_effect > 0 && !target.hp.atMax()){
        return EffectStatus::useful;
    }
    if(mp_effect > 0 && !target.mp.atMax()){
        return EffectStatus::useful;
    }
    if(ap_effect > 0 && !target.ap.atMax()){
        return EffectStatus::useful;
    }

    return EffectStatus::pointless;
}

void BuffEffect::activate(Character& source, World&){
    source.buffs.push_back(buffgen(source));
}
