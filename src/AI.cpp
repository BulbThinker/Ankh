#include "../include/allegro.h"

#include "../include/AI.hpp"
#include "../include/util.hpp"
#include "../include/logger.hpp"

int directionToKey(Direction dir){
    switch(dir){
    case _up:
        return ALLEGRO_KEY_UP;
    case _left:
        return ALLEGRO_KEY_LEFT;
    case _right:
        return ALLEGRO_KEY_RIGHT;
    case _down:
        return ALLEGRO_KEY_DOWN;
    case _stop:
        return ALLEGRO_KEY_DOWN;
    }
    Logger::getI().criticalError("Invalid direction for directionToKey");
    return 0;
}

void RandomAI::EnemyIA(PlayerCharacter* per, std::vector<AICharacter<RandomAI>*> hostiles, World& world){
    std::unique_ptr<Ability>& punch = *(aichar->activate(ALLEGRO_KEY_P));

    if(punch->active(*per, hostiles, world)){
        std::vector<Character*> targets;
        bool doneTargeting = punch->calculateTargets(targets, *per, hostiles, world);

        if(doneTargeting){
            Logger::getI().criticalError("Punch is pretargeted");
        }
        if(!targets.empty()){
            punch->insertTarget(targets[0], world);
            punch->activate(world, *per, hostiles);
            return;
        }
    }

    std::uniform_int_distribution<> dis1(0,1);

    switch(dis1(gen)){
    case 0:
        (*(aichar->activate(directionToKey(aichar->closestDirection(*per)))))->activate(world, *per, hostiles);
        break;
    case 1:
        std::uniform_int_distribution<> dis2(0,8);
        switch(dis2(gen)){
        case 0:
        case 1:
            (*(aichar->activate(ALLEGRO_KEY_UP)))->activate(world, *per, hostiles);
            break;
        case 2:
        case 3:
            (*(aichar->activate(ALLEGRO_KEY_LEFT)))->activate(world, *per, hostiles);
            break;
        case 4:
        case 5:
            (*(aichar->activate(ALLEGRO_KEY_DOWN)))->activate(world, *per, hostiles);
            break;
        case 6:
        case 7:
            (*(aichar->activate(ALLEGRO_KEY_RIGHT)))->activate(world, *per, hostiles);
            break;
        case 8:
            (*(aichar->activate(ALLEGRO_KEY_E)))->activate(world, *per, hostiles);
            break;
        }
        break;
    }



}
