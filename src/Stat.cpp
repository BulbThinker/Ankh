#include "../include/Stat.hpp"
#include "../include/util.hpp"

template <typename T> Number<T>::operator T (){
  return value;
}

template <typename T> bool Number<T>::operator == (const T o) const {
    return value == o;
}
template <typename T> bool Number<T>::operator != (const T o) const {
    return value != o;
}
template <typename T> bool Number<T>::operator <= (const T o) const {
    return value <= o;
}
template <typename T> bool Number<T>::operator < (const T o) const {
    return value < o;
}
template <typename T> bool Number<T>::operator > (const T o) const {
    return value > o;
}
template <typename T> bool Number<T>::operator >= (const T o) const {
    return value >= o;
}

template <typename T> void Number<T>::operator += (const T o){
    value += o;
}
template <typename T> void Number<T>::operator -= (const T o){
    value -= o;
}

template <typename T> T Number<T>::operator + (const T o) const {
    return value + o;
}

template <typename T> void Stat<T>::operator += (const T o){
    this->value = max(0, min(this->value+o, int(maxValue)));
}
template <typename T> void Stat<T>::operator -= (const T o){
    this->value = max(0, min(this->value-o, int(maxValue)));
}

template <typename T> void Number<T>::operator = (const T o){
    this->value = o;
}

template <typename T> void Stat<T>::operator = (const T o){
    this->value = max(0, min(o, int(maxValue)));
}

template <typename T> std::string Number<T>::toString() const {
    return convertToString(value);
}

template <typename T> std::string Stat<T>::toString() const {
    stringstream o;
    o << convertToString(this->value) << "/" << convertToString(maxValue);
    return o.str();
}

template <typename T> void Stat<T>::bDiff(const T o){
    maxValue += o;
    this->value += o;
}

template <typename T> void Stat<T>::setToMax(){
    this->value = maxValue;
}

template <typename T> bool Stat<T>::atMax() const {
    return maxValue == this->value;
}

template <typename T> void Stat<T>::draw(ALLEGRO_FONT* ability_font, int posX, int posY, std::string prefix) const {
    draw_thing_text(ability_font, posX, posY, prefix += toString());
}

template class Number<int>;
template class Stat<int>;

