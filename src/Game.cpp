#include "../include/Game.hpp"

#include "../include/logger.hpp"

#include <allegro5/allegro_native_dialog.h>


void Game::startGame(){
    isGUIopen = false;
    isPaused = false;
    openGUI = nullptr;
}

void Game::endGame(){;
    loop = false;
}


void Game::start(bool fullreset)
{

    cout<<"Restarting Game Deleting: "<<*(mundo.getNumEntities())<<" Entities"<<endl;
    for(auto h = hostiles.begin();h!=hostiles.end();h++){
        delete (*h);
    }
    hostiles.clear();
    delete (per);
    if(fullreset){
      level = 0;
      killcount = 0;
    }
    turn = 0;
    isPlayerTurn = true;
    state = playing;

    mundo.ResetID();

    per = PlayerCharacter::def_pc(mundo, "character", Coord{0,0}, 10, 10, 5, 10, 10, 5);

    int remaining_enemies = ENEMYNUMBER;
    while(remaining_enemies--){
        hostiles.push_back(AICharacter<RandomAI>::random_monster(mundo));
    }

    CASv.clear();

    cout<<"Restarting Game Load: "<<*(mundo.getNumEntities())<<" Entities"<<endl;
}

void Game::restart(bool fullreset)
{
    start(fullreset);
    startGame();
}

void drawgame(ALLEGRO_DISPLAY * const display, Screen& tela, const World& mundo, PlayerCharacter& per, const std::vector<AICharacter<RandomAI>*>& hostiles, const State& state, const std::vector<Character*>& targets, const int turn, const int level, const int killcount, ALLEGRO_FONT * big_font, ALLEGRO_FONT * ability_font, ALLEGRO_FONT * target_font){
    al_set_target_backbuffer(display);
    al_clear_to_color(al_map_rgb(0, 0, 0));
    tela.selectVisible();
    tela.updatePosition(per);
    tela.draw();
    //per.drawSprite(Coord{0,0});

    //draw status
    per.hp.draw(ability_font, 0, 0, "Hp: ");
    per.mp.draw(ability_font, 0, 20, "Mp: ");
    per.ap.draw(ability_font, 0, 40, "Ap: ");

    std::stringstream textt;
    textt << "Turn: ";
    textt << turn;
    textt << ", Level: ";
    textt << level;
    textt << ", Kills: ";
    textt << killcount;

    draw_thing_text(ability_font, CONFIG::screen_w-190, 0, textt, al_map_rgba(255,200,200,0), ALLEGRO_ALIGN_LEFT);


    int posy = 0;
    //draw abilities
    for(auto a = per.abilities.begin(); a != per.abilities.end(); a++){
        if((*a)->show){
            (*a)->drawEA(ability_font, 100, posy, per, hostiles, mundo);
            posy += 20;
        }
    }
    posy = 0;
    for(auto a = per.buffs.begin(); a != per.buffs.end(); a++){
        if((*a)->show){
            (*a)->drawEA(ability_font, 200, posy);
            posy += 20;
        }
    }

    if(state == targeting){
        draw_thing_text(big_font, CONFIG::screen_w/2, CONFIG::screen_h/2-30, "Select Target", al_map_rgba(255,200,200,0), ALLEGRO_ALIGN_CENTER);

        std::string letter = "A";
        for(auto t = targets.begin(); t != targets.end(); t++){
            (*t)->drawOnTop(letter.c_str(), target_font, coord{0, 0}, al_map_rgba(255, 0, 0, 255));
            letter[0]++;
        }
    }

    if (state == gameover){
        stringstream end_message;
        end_message << "You survived to turn " << turn << ", on level: " << level;
        draw_thing_text(big_font, CONFIG::screen_w/2, CONFIG::screen_h/2-30, "GAME OVER", al_map_rgba(255,200,200,0), ALLEGRO_ALIGN_CENTER);

        draw_thing_text(big_font, CONFIG::screen_w/2, CONFIG::screen_h/2+30, end_message, al_map_rgba(255,200,200,0), ALLEGRO_ALIGN_CENTER);

        stringstream killcount_message;
        killcount_message << "Killcount: " << killcount;
        draw_thing_text(big_font, CONFIG::screen_w/2, CONFIG::screen_h/2+80, killcount_message, al_map_rgba(255,200,200,0), ALLEGRO_ALIGN_CENTER);
    }

    tela.clearBuffer();
}

void checkend(PlayerCharacter& per, State& state){
    if(per.hp <= 0){
        state = gameover;
    }
}

void gameinput(int keycode, PlayerCharacter& per, State& state, std::unique_ptr<Ability>*& selected_ability, World& mundo, std::vector<AICharacter<RandomAI>*>& hostiles, std::vector<Character*>& targets){
    if(state == playing){
        selected_ability = per.activate(keycode);
        if(selected_ability){
            if((*selected_ability)->active(per, hostiles, mundo)) {
                bool doneTargeting = (*selected_ability)->calculateTargets(targets, per, hostiles, mundo);
                if(doneTargeting){
                    (*selected_ability)->activate(mundo, per, hostiles); // activate now
                    selected_ability = nullptr;
                } else if (targets.empty()) {
                    selected_ability = nullptr; // no valid target, give up
                } else {
                    (*selected_ability)->selected = true;
                    state = targeting;
                }
            } else {
                selected_ability = nullptr;
            }
        }
    } else if (state == targeting){
        int tar_int = keycode - ALLEGRO_KEY_A;
        if(tar_int < int(targets.size())) {
            (*selected_ability)->insertTarget(targets[tar_int], mundo);

            bool doneTargeting = (*selected_ability)->calculateTargets(targets, per, hostiles, mundo);
            if(doneTargeting){
                (*selected_ability)->activate(mundo, per, hostiles); // activate now
                selected_ability = nullptr;
                state = playing;
            } else if (targets.empty()) {
                (*selected_ability)->reset();
                selected_ability = nullptr; // no valid target, give up
                state = playing;
            } else {
                state = targeting;
            }
        }
    }
}


void Game::mainloop(){

    while (loop) {
        ALLEGRO_EVENT ev;
        al_wait_for_event(event_queue, &ev);
        ///EVENTOS DE TIMER
        if (ev.type == ALLEGRO_EVENT_TIMER){
            ///TIMER UPDATE_SCREEN
            if (ev.timer.source == screen_update_timer) {
                frameCounter = (frameCounter + 1) % 300;
                redraw = true;
            }
            ///TIMER DE FLUXO
            if(ev.timer.source == tu_timer && !isPaused){
                al_stop_timer(tu_timer);

                CASv.erase(std::remove_if(CASv.begin(), CASv.end(), [](unique_ptr<CarryAnimationState>& cas){return cas->activate();}), CASv.end());

                al_set_timer_count(tu_timer,0);

                if(CASv.empty()){

                    if(isPlayerTurn){
                        if(per->ap <= 0){
                            turn++;
                            (*per).end_turn(turn, mundo);
                            isPlayerTurn = false;
                        }
                    }

                    std::vector<AICharacter<RandomAI>*> deadhostiles (hostiles.size());
                    deadhostiles.resize(std::distance(deadhostiles.begin(), std::copy_if(hostiles.begin(), hostiles.end(), deadhostiles.begin(), [](AICharacter<RandomAI>* aic){return aic->hp <= 0;})));
                    killcount += deadhostiles.size();
                    hostiles.resize(std::remove_if(hostiles.begin(), hostiles.end(), [](AICharacter<RandomAI>* aic){return aic->hp <= 0;}) - hostiles.begin());

                    for(auto h = deadhostiles.begin(); h < deadhostiles.end(); h++){
                        delete *h;
                    }

                    deadhostiles.clear();


                    if (!isPlayerTurn){

                        bool ai_end = true;

                        for(auto h = hostiles.begin(); h != hostiles.end(); h++){
                            if((*h)->ap > 0){
                                (*h)->ai.EnemyIA(per, hostiles, mundo);
                                ai_end = false;
                                break;
                            }
                        }

                        if (ai_end) {
                            for(auto h = hostiles.begin(); h != hostiles.end(); h++){
                                (*h)->end_turn(turn, mundo);
                            }

                            if(hostiles.empty()){
                               level++;
                               restart(false);
                            }

                            if(((3.0*generate_enemies_chance) > rand_create(gen)) && (hostiles.size() < 20)){
                                hostiles.push_back(AICharacter<RandomAI>::level_monster(mundo, level));
                            }
                            isPlayerTurn = true;
                        }
                    }
                }

                checkend(*per, state);

                al_set_timer_speed(tu_timer, 1.0/(100.0+hostiles.size()*25.0));
                al_set_timer_count(tu_timer,0);
                al_start_timer(tu_timer);

                if(state == gameover){
                    isPaused = true;
                    isGUIopen = true;
                    changeToGui(openGUI, (*guiLibrary)[GUIL::ENDGAME]);
                }
            }
        }
        else if (ev.type == ALLEGRO_EVENT_KEY_DOWN) {
            if(ev.keyboard.keycode == ALLEGRO_KEY_ESCAPE){
                if(isGUIopen){
                    isPaused = false;
                    isGUIopen = false;
                    openGUI = NULL;
                }else{
                    isPaused = true;
                    isGUIopen = true;
                    openGUI = (*guiLibrary)[GUIL::PAUSE_MENU];
                }
            }
            else if(CASv.empty() && !isGUIopen && isPlayerTurn){
                gameinput(ev.keyboard.keycode, *per, state, selected_ability, mundo, hostiles, targets);
                checkend(*per, state);
            }
        } else if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
            isPaused = true;
            al_set_audio_stream_playing(audio_stream,false);
            if(al_show_native_message_box(display,ANKH_APP_NAME,"","Are you sure you want to leave Ankh?",NULL,ALLEGRO_MESSAGEBOX_YES_NO) == 1){
               loop = false;
            }
            else{
                isPaused = false;
                al_set_audio_stream_playing(audio_stream,true);
            }
        }
        else if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN){
            if(isGUIopen && openGUI->isClickableGUI){
                openGUI->ClickIn(ev.mouse.x,ev.mouse.y);
            }
        }
        else if (ev.type == ALLEGRO_EVENT_KEY_CHAR){
            if(isGUIopen && openGUI->isClickableGUI){
                openGUI->UnicharReceive(ev.keyboard.unichar);
                std::cout << ev.keyboard.unichar << "\n";
            }
        }
        else if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP){
            if(isGUIopen && openGUI->isClickableGUI){
                openGUI->ClickOut();
            }
        }
        else if(ev.type == ALLEGRO_EVENT_MOUSE_AXES){
            if(isGUIopen && openGUI->isMouseUpdateGUI){
                openGUI->move(ev.mouse.x,ev.mouse.y);
                al_set_audio_stream_gain(audio_stream,CONFIG::gain);
            }
        }
        if (redraw && al_is_event_queue_empty(event_queue)) {
            redraw = false;
            if(!isGUIopen){
                drawgame(display, tela, mundo, *per, hostiles, state, targets, turn, level, killcount, big_font, ability_font, target_font);
            }
            else{
                al_set_target_backbuffer(display);
                al_clear_to_color(al_map_rgb(0, 0, 0));
                openGUI->draw();
                al_flip_display();
            }
        }
    }
}


void Game::loadgame(sqlite3* db, int id){
}
void Game::storegame(sqlite3* db, int slot, std::string& name){
}

Game::~Game(){
    delete (per);
    for(auto h = hostiles.begin();h!=hostiles.end();h++){
        delete (*h);
    }
}

Game::Game(
        bool& _isGUIopen,
        GUI*& _openGUI,
        ALLEGRO_TIMER* _screen_update_timer, //receber?
        ALLEGRO_TIMER* _tu_timer, //receber?

        ALLEGRO_FONT* _ability_font,// receber
        ALLEGRO_FONT* _target_font,// receber
        ALLEGRO_FONT* _big_font,// receber

        ALLEGRO_EVENT_QUEUE* _event_queue, // receber

        ALLEGRO_AUDIO_STREAM* _audio_stream,

        ALLEGRO_DISPLAY* _display,

        LIB_GUI* _guiLibrary // receber
           ):
    mundo("back.png", 20, 15),
    tela(mundo, CONFIG::screen_w, CONFIG::screen_h),
    isGUIopen(_isGUIopen),
    openGUI(_openGUI),
 screen_update_timer(_screen_update_timer), //receber?
 tu_timer(_tu_timer), //receber?

 ability_font(_ability_font),// receber
 target_font(_target_font),// receber
 big_font(_big_font),// receber

 event_queue(_event_queue), // receber

 audio_stream(_audio_stream),

 display(_display),

 guiLibrary(_guiLibrary) // receber
    {
    level = 0;
    turn = 0;
    killcount = 0;

    hostiles.clear();

    state = playing;
    per = nullptr;

    selected_ability = nullptr;

    generate_enemies_chance = 0.1;

    targets.clear();

    start();

    }
