/**============================================================================
 *  Project Name    <Ankh>
 *  Version         <1.0>
 *  Authors         <all members>
 *  File Name       <GUI.cpp>
/=============================================================================*/
#include "../include/GUI.hpp"
#include "../include/logger.hpp"

/**==========================================================================**/
using namespace std;



void changeToGui(GUI*& opGUI, GUI* toGUI){
  opGUI = toGUI;
  opGUI->reset();
}

/**==========================================================================**/
///BUTTON
BUTTON::BUTTON(int _px, int _py,int _w,int _h,ALLEGRO_BITMAP* _background,
               ALLEGRO_FONT* _font,ALLEGRO_COLOR _color,string _text, std::function<void()> _inFunction)
    :GUIELEMENT(_px,_py,_w,_h,true,false),
     background(_background),font(_font),color(_color),
     inFunction(_inFunction),text(_text){
}

BUTTON::~BUTTON(){
    cout<<"Deleting "<<this->text<<endl<<" Button";
}

void BUTTON::draw(int& ox,int& oy){
    al_draw_scaled_bitmap(this->background,0,0,
                          al_get_bitmap_width(this->background),
                          al_get_bitmap_height(this->background),ox+this->pX,
                          oy+this->pY,this->w,this->h,0);
    draw_thing_text(this->font,
                        ox+this->pX+this->w/2,oy+this->pY+this->h/2,
                        this->text.data(),this->color,
                        ALLEGRO_ALIGN_CENTER);
}

void BUTTON::reset(){
}

void BUTTON::UnicharReceive(int uch){
}

///TEXT
TEXT::TEXT(int _px, int _py,int _w,int _h, ALLEGRO_FONT* _font,
           ALLEGRO_COLOR _textcolor, std::string _deftext)
    :GUIELEMENT(_px,_py,_w,_h,true,false),
     font(_font),textcolor(_textcolor),
     text(_deftext){
}

TEXT::~TEXT(){
    cout<<"Deleting "<<this->text<<endl<<" TEXT";
}

void TEXT::draw(int& ox,int& oy){
    draw_thing_text(this->font,ox+this->pX+5,oy+this->pY+4,
                    text.c_str(),al_map_rgb(255,255,255),
                    ALLEGRO_ALIGN_LEFT);
}


/**==========================================================================**/


///TEXTBOX
TEXTBOX::TEXTBOX(int _px, int _py,int _w,int _h,
               ALLEGRO_FONT* _font,ALLEGRO_COLOR _backcolor,ALLEGRO_COLOR _textcolor,std::string _deftext)
  :TEXT(_px,_py,_w,_h, _font, _textcolor, _deftext),
  backcolor(_backcolor),
  deftext(_deftext){
  isPress = true;
  isSelected = false;
}

TEXTBOX::~TEXTBOX(){
    cout<<"Deleting "<<this->text<<endl<<" TEXTBOX";
}

void TEXTBOX::draw(int& ox,int& oy){
//    al_draw_filled_rectangle(  ox+this->pX+5,oy+this->pY+4,ox+this->pX+8       ,this->h+oy+this->pY-4, al_map_rgb(255,255,255));
    al_draw_rectangle       (  ox+this->pX  ,oy+this->pY  ,this->w+ox+this->pX ,this->h+oy+this->pY  , backcolor,5);
    std::string ntext{text};
    ntext += "|";
    draw_thing_text(this->font,ox+this->pX+5,oy+this->pY+4,
                             ntext,al_map_rgb(255,255,255),
                             ALLEGRO_ALIGN_LEFT);
    //TODO
   /* draw_thing_text(this->font,
                        ox+this->pX+this->w/2,oy+this->pY+this->h/2,
                        this->text.data(),this->color,
                        ALLEGRO_ALIGN_CENTER); */
}

void TEXTBOX::ClickIn(){
    text = "";
    isSelected = true;
}

void TEXTBOX::reset(){
    this->text = deftext;
}

void TEXTBOX::UnicharReceive(int uch){
    if(uch == '\b'){
        if(text.size()){
            text.resize(text.size()-1);
        }
    } else if (isprint(uch)) {
        this->text += uch;
    }
}
/**==========================================================================**/
///TABLE
template <size_t COL>
TABLE<COL>::TABLE(
                  int _px, int _py,int _w,int _h,ALLEGRO_FONT* _font,
                  ALLEGRO_COLOR _backcolor,ALLEGRO_COLOR _textcolor,
                  std::array<std::pair<std::string,int>,COL> _cols,
                  std::function<std::vector<std::array<std::unique_ptr<GUIELEMENT>,COL>>()> _genline,
                  int _line_height
                  ):GUIELEMENT(_px,_py,_w,_h,true,true), font(_font),
                    backcolor(_backcolor),textcolor(_textcolor),cols(_cols),
                    genline(_genline),line_height(_line_height){}

template <size_t COL>
void TABLE<COL>::draw(int& ox,int& oy){

  int dify = pY+30;

  int difx = pX+0;
  for(auto& col: cols){
    int noy = dify+oy;
    int nox = difx+ox;
    draw_thing_text(this->font,
                    nox,noy,
                    std::get<0>(col),this->textcolor,
                    ALLEGRO_ALIGN_LEFT);
    difx += std::get<1>(col);
  }
  dify += line_height;

  for(auto& line: elements){
    int difx = pX+0;
    for(unsigned int i = 0; i < line.size(); i++){
      auto& el = line.at(i);
      auto& col = cols.at(i);
      int noy = dify+oy;
      int nox = difx+ox;
      el->draw(nox, noy);
      difx += std::get<1>(col);
    }
    dify += line_height;
  }
}

template <size_t COL>
void TABLE<COL>::reset(){
  elements = genline();
}

template <size_t COL>
void TABLE<COL>::ClickIn(){
  //todo
}
template <size_t COL>
void TABLE<COL>::UnicharReceive(int ch){
  //todo
}

/**==========================================================================**/
///SLIDEBAR
SLIDEBAR::SLIDEBAR(int _px, int _py,int _w,int _h,int _wc,ALLEGRO_BITMAP* _cursor,
                      ALLEGRO_BITMAP* _background,bool _isHorizontal,
                      double& value,ALLEGRO_FONT* _font,
                      ALLEGRO_COLOR _color, std::string _text):GUIELEMENT(_px,_py,_w,_h,true,true),
                      cursor(_cursor),background(_background),
                      isHorizontal(_isHorizontal),
                      value(value),wc(_wc),color(_color),font(_font),text(_text){}

void SLIDEBAR::draw(int& ox,int& oy){
    al_draw_scaled_bitmap(this->background,0,0,
                              al_get_bitmap_width(this->background),
                              al_get_bitmap_height(this->background),
                              this->pX+ox,this->pY+oy,this->w,this->h,0);
    if(this->isHorizontal){
        draw_thing_text(this->font,
                        ox+this->pX+this->w/2,(-30)+oy+this->pY+this->h/2,
                        this->text.data(),this->color,
                        ALLEGRO_ALIGN_CENTER);
        al_draw_scaled_bitmap(this->cursor,0,0,
                              al_get_bitmap_width(this->cursor),
                              al_get_bitmap_height(this->cursor),
                              this->pX+ox+(this->value*(this->w-this->wc)),this->pY+oy,this->wc,this->h,0);
    }
    else{
        al_draw_scaled_bitmap(this->cursor,0,0,
                              al_get_bitmap_width(this->cursor),
                              al_get_bitmap_height(this->cursor),
                              this->pX+ox,this->pY+oy+(this->value*(this->h-this->wc)),this->w,this->wc,0);
    }
}

void SLIDEBAR::move(int& mX,int& mY,int& oX,int& oY){
    if(!this->isPress)
        return;
    if(this->isHorizontal){
        this->value = ((mX-(this->pX+oX))/(double)(this->w-this->wc));
    }
    else{
        this->value = ((mY-(this->pY+oY))/(double)(this->h-this->wc));
    }
    if(this->value < 0.0)
        this->value = 0.0;
    else if(this->value > 1.0)
        this->value = 1.0;
}

void SLIDEBAR::reset(){
}

void SLIDEBAR::UnicharReceive(int uch){
}

/**==========================================================================**/
///GUI
GUI::GUI(string _id,int _w,int _h,ALLEGRO_BITMAP* _background,
        bool _isClickable,bool _isMouseUpdateGui,std::function<void()> _extraDraw):background(_background),
        w(_w),h(_h),isClickableGUI(_isClickable),
        isMouseUpdateGUI(_isMouseUpdateGui),extraDraw(_extraDraw),id(_id),elements(){}


void GUI::draw(void){
    int ox = ((CONFIG::screen_w/2)-(this->w/2));
    int oy = ((CONFIG::screen_h/2)-(this->h/2));
    al_draw_scaled_bitmap(this->background,0,0,
                          al_get_bitmap_width(this->background),
                          al_get_bitmap_height(this->background),ox,oy,
                          this->w,this->h,0);

    for(auto& el: elements){
        if(el){
                el->draw(ox, oy);
        }
    }

    if(extraDraw) {
        extraDraw();
    }
    return;


}
// TODO: better loop.
void GUI::ClickIn(int mX,int mY){
    ResetSelection();
    vector<unique_ptr<GUIELEMENT>>::iterator itr;
    GUIELEMENT* ge;
    int ox = ((CONFIG::screen_w/2)-(this->w/2));
    int oy = ((CONFIG::screen_h/2)-(this->h/2));
    unsigned int i;
    for(itr=(this->elements.begin()),i=0;itr!=(this->elements.end());itr++,i++){
        ge = elements[i].get();
        if(ge->isClickable && (mX >= ge->pX+ox) && (mX <= ge->pX + ge->w + ox) &&
           (mY >= ge->pY + oy) && (mY <= ge->pY + ge->h  + oy)){
            ge->isPress = true;
            ge->isSelected = true;
            ge->ClickIn();
        }
    }
}

void GUI::ClickOut(void){
    for(auto& ge: elements){
        if(ge->isPress){
            ge->isPress = false;
        }
    }
}

void GUI::UnicharReceive(int uch){
    for(auto& ge: elements){
        if(ge->isSelected){
            ge->UnicharReceive(uch);
        }
    }
}

void GUI::move(int mX,int mY){
    vector<unique_ptr<GUIELEMENT>>::iterator itr;
    int ox = ((CONFIG::screen_w/2)-(this->w/2));
    int oy = ((CONFIG::screen_h/2)-(this->h/2));
    GUIELEMENT* ge;
    for(itr=(this->elements.begin());itr!=(this->elements.end());itr++){
        ge = (*itr).get();
        if(ge->isMouseUpdate)
            ge->move(mX,mY,ox,oy);
    }
}

void GUI::add(GUIELEMENT* ge){
    if(ge){
        this->elements.push_back((unique_ptr<GUIELEMENT>)ge);
    } else {
        Logger::getI() << "Error null guielement";
    }
}

void GUI::reset(){
    ResetSelection();
    for(auto& el: elements){
        if(el){
            el->reset();
        }
    }
}

void GUI::ResetSelection(void){
    for(auto& ge: elements){
        if(ge->isSelected){
            ge->isSelected = false;
        }
    }
}
/**==========================================================================**/
