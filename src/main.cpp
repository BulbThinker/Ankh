/**============================================================================
 *  Project Name    <Ankh>
 *  Version         <1.0>
 *  Authors         <all members>
 *  File Name       <main.cpp>
/=============================================================================*/
#include <stdlib.h>
#include <sstream>
#include <allegro5/allegro_native_dialog.h>
#include <sqlite3.h>

#include "../include/Ankh.hpp"
#include "../include/util.hpp"
#include "../include/logger.hpp"
#include "../include/logger.hpp"
#include "../include/GUI.hpp"
#include "../include/Entity.hpp"
#include "../include/Character.hpp"
#include "../include/LIB.hpp"
#include "../include/Game.hpp"
#include "../include/AI.hpp"
#include "../include/SFX_PLAYER.hpp"
/**==========================================================================**/

/**==========================================================================**/
using namespace std;
/**==========================================================================**/

LIB_BITMAP bmpLibrary;
LIB_GUI guiLibrary;
/**==========================================================================**/
///Prototipos de Funcoes
/**==========================================================================**/
  static int callback_to_log(void *, int argc, char **argv, char **azColName){
    for(int i=0; i<argc; i++){
      printf("%d/%d: %s = %s\n", i,argc, azColName[i], argv[i] ? argv[i] : "NULL");
    }
   printf("\n");
    return 0;
  }

static int callback_insert_vec(void *ivec, int argc, char **argv, char **azColName){
  std::vector<std::vector<std::string>>* vec = static_cast<std::vector<std::vector<std::string>>*>(ivec);
  vec->emplace_back();
  auto tvec = vec->end()-1;
  for(int i=0; i<argc; i++){
    tvec->push_back(argv[i]);
  }
  return 0;
}

void savescore(GUI*& opGUI, GUI* toGUI, sqlite3 *db, std::string& text, std::unique_ptr<Game>& game) {

  int row = 0;
  char* zErrMsg = 0;
  std::stringstream statement {};
  statement << "INSERT INTO scores (name, turn, level, kills) VALUES (";
  statement << "\"" << text << "\"" << "," << game->turn << "," << game->level << "," << game->killcount << ")";
  Logger::getI() << statement.str();
  std::cout << statement.str();
  int rc = sqlite3_exec(db, statement.str().c_str(), callback_to_log, &row, &zErrMsg);
  if( rc!=SQLITE_OK ){
    Logger::getI() << "SQL error: ";
    Logger::getI() << zErrMsg;
    sqlite3_free(zErrMsg);
  }

  changeToGui(opGUI, toGUI);
}

void loadgame(sqlite3 *db, int id, std::unique_ptr<Game>& game) {
  game->loadgame(db,id);
  game->isGUIopen = false;
}

void storegame(GUI* toGUI, sqlite3 *db, int slot, std::string& name, std::unique_ptr<Game>& game) {
  game->storegame(db,slot,name);
  changeToGui(game->openGUI, toGUI);
}

TABLE<4>* generate_hs_table(sqlite3* db, ALLEGRO_FONT* font){
  return new TABLE<4>(50,120,200,200,font,al_map_rgb(255,255,255),al_map_rgb(255,255,255),
                      {std::make_pair("Player",100),std::make_pair("Turn",100),std::make_pair("Level",100),std::make_pair("Kills",100)}
                      ,[db, font](){
                        std::vector<std::vector<std::string>> highscorestext{};
                        char* zErrMsg = 0;
                        int rc = sqlite3_exec(db, "SELECT name,turn,level,kills FROM \"SCORES\" ORDER BY level DESC, kills DESC LIMIT 5;", callback_insert_vec, &highscorestext, &zErrMsg);
                        if( rc!=SQLITE_OK ){
                          Logger::getI() << "SQL error: ";
                          Logger::getI() << zErrMsg;
                          sqlite3_free(zErrMsg);
                        }

                        std::vector<std::array<std::unique_ptr<GUIELEMENT>,4>> outtt;
                        for(auto& linedata: highscorestext){
                          outtt.emplace_back();
                          auto outline = outtt.end()-1;
                          for(int i = 0; i<4; i++){
                            (*outline)[i] = std::unique_ptr<GUIELEMENT>(new TEXT(0,0,20,20,font,al_map_rgba(100,100,100,0), linedata.at(i)));
                          }
                        }
                        return outtt;
                      }, 50);
}

TABLE<3>* generate_load_table(sqlite3* db, ALLEGRO_FONT* font, std::unique_ptr<Game>& game){
  return new TABLE<3>(50,120,200,200,font,al_map_rgb(255,255,255),al_map_rgb(255,255,255),
                      {std::make_pair("Name",150),std::make_pair("Level",100),std::make_pair("",200)} // TODO: button here
                      ,[db, font, &game](){
                        std::vector<std::vector<std::string>> savestext{};
                        char* zErrMsg = 0;
                        int rc = sqlite3_exec(db, "SELECT name,level FROM \"saves\" ORDER BY id;", callback_insert_vec, &savestext, &zErrMsg);
                        if( rc!=SQLITE_OK ){
                          Logger::getI() << "SQL error: ";
                          Logger::getI() << zErrMsg;
                          sqlite3_free(zErrMsg);
                        }

                        std::vector<std::array<std::unique_ptr<GUIELEMENT>,3>> outtt;
                        for(unsigned int linei = 0; linei < savestext.size(); linei++){
                          auto& linedata = savestext.at(linei);
                          outtt.emplace_back();
                          auto outline = outtt.end()-1;
                          for(int i = 0; i<2; i++){
                            (*outline)[i] = std::unique_ptr<GUIELEMENT>(new TEXT(0,0,20,20,font,al_map_rgba(100,100,100,0), linedata.at(i)));
                          }
                          (*outline)[2] = std::unique_ptr<GUIELEMENT>(new BUTTON(10,10,180,80,bmpLibrary[BMPL::BUTTON],font,al_map_rgba(100,100,100,0), "LOAD",
                                                                                 std::bind([db, linei, &game](){
                                                                                     loadgame(db, linei, game);
                                                                                   })));
                        }
                        return outtt;
                      }, 80);
}

TABLE<4>* generate_saveload_table(sqlite3* db, ALLEGRO_FONT* font, std::unique_ptr<Game>& game, GUI* after_save_menu){
  return new TABLE<4>(50,120,200,200,font,al_map_rgb(255,255,255),al_map_rgb(255,255,255),
                      {std::make_pair("Name",150),std::make_pair("Level",100),std::make_pair("",200)} // TODO: button here
                      ,[db, font, &game, after_save_menu](){
                        std::vector<std::vector<std::string>> savestext{};
                        char* zErrMsg = 0;
                        int rc = sqlite3_exec(db, "SELECT name,level FROM \"saves\" ORDER BY id;", callback_insert_vec, &savestext, &zErrMsg);
                        if( rc!=SQLITE_OK ){
                          Logger::getI() << "SQL error: ";
                          Logger::getI() << zErrMsg;
                          sqlite3_free(zErrMsg);
                        }

                        std::vector<std::array<std::unique_ptr<GUIELEMENT>,4>> outtt;
                        for(unsigned int linei = 0; linei < savestext.size(); linei++){
                          auto& linedata = savestext.at(linei);
                          outtt.emplace_back();
                          auto outline = outtt.end()-1;
                          for(int i = 0; i<2; i++){
                            (*outline)[i] = std::unique_ptr<GUIELEMENT>(new TEXT(0,0,20,20,font,al_map_rgba(100,100,100,0), linedata.at(i)));
                          }
                          (*outline)[2] = std::unique_ptr<GUIELEMENT>(new BUTTON(10,10,180,80,bmpLibrary[BMPL::BUTTON],font,al_map_rgba(100,100,100,0), "LOAD",
                                                                                 std::bind([db, linei, &game](){
                                                                                     loadgame(db, linei, game);
                                                                                   })));
                          (*outline)[3] = std::unique_ptr<GUIELEMENT>(new BUTTON(10,10,180,80,bmpLibrary[BMPL::BUTTON],font,al_map_rgba(100,100,100,0), "SAVE",
                                                                                 std::bind([db, linei, &game, after_save_menu](){
                                                                                     std::string name {"NONAME"};
                                                                                     storegame(after_save_menu,db, linei, name, game); // TODO
                                                                                   })));
                        }
                        return outtt;
                      }, 80);
}

void initGuiLib(LIB_GUI& guiLibrary, ALLEGRO_FONT* ability_font, ALLEGRO_FONT* big_font, std::unique_ptr<Game>& game, sqlite3* db, GUI*& openGUI){

        ///GUIS
    guiLibrary.add(new GUI("MAIN_MENU",640,480,bmpLibrary[BMPL::MAIN_MENU],true,false));
    guiLibrary.add(new GUI("PAUSE_MENU",640,480,bmpLibrary[BMPL::MENU],true,false));
    guiLibrary.add(new GUI("CONFIG_MENU",640,480,bmpLibrary[BMPL::MENU],true,true));
    guiLibrary.add(new GUI("CREDITOS",640,480,bmpLibrary[BMPL::CREDITOS],true,false));
    guiLibrary.add(new GUI("HIGHSCORES",640,480,bmpLibrary[BMPL::MENU],true,false));
    guiLibrary.add(new GUI("LOAD",640,480,bmpLibrary[BMPL::MENU],true,false));
    guiLibrary.add(new GUI("SAVELOAD",640,480,bmpLibrary[BMPL::MENU],true,false));
    guiLibrary.add(new GUI("MMCONFIG",640,480,bmpLibrary[BMPL::MENU],true,true));
    guiLibrary.add(new GUI("ENDGAME",640,480,bmpLibrary[BMPL::MENU],true,false,[db,&game,big_font,ability_font](){
        std::stringstream textt{};
        textt << "Turn: " << game->turn << ", Level: " << game->level << ", Kills: " << game->killcount;

        draw_thing_text(big_font,
                        300,10,
                        "GAME OVER",al_map_rgba(255,100,100,0),
                        ALLEGRO_ALIGN_CENTER);
        draw_thing_text(big_font,
                        300,50,
                        textt,al_map_rgba(255,100,100,0),
                        ALLEGRO_ALIGN_CENTER);
    }));

        ///Main_Menu
    guiLibrary[GUIL::MAIN_MENU]->add(new BUTTON(150,150,200,100,bmpLibrary[BMPL::BUTTON],ability_font,al_map_rgb(255,255,255),"NEW GAME",std::bind([&game](){game->restart(true);})));///restart,std::ref(openGUI),std::ref(isGUIopen),std::ref(isPaused),std::ref(isPlayerTurn),std::ref(hostiles),std::ref(per),std::ref(mundo),std::ref(state),std::ref(level),std::ref(killcount),std::ref(turn),true)));
    guiLibrary[GUIL::MAIN_MENU]->add(new BUTTON(150,250,200,100,bmpLibrary[BMPL::BUTTON],ability_font,al_map_rgb(255,255,255),"CREDITS",std::bind(changeToGui,std::ref(openGUI),guiLibrary[GUIL::CREDITOS])));
    guiLibrary[GUIL::MAIN_MENU]->add(new BUTTON(150,350,200,100,bmpLibrary[BMPL::BUTTON],ability_font,al_map_rgb(255,255,255),"LOAD GAME",std::bind(changeToGui,std::ref(openGUI),guiLibrary[GUIL::LOAD])));
    guiLibrary[GUIL::MAIN_MENU]->add(new BUTTON(350,150,200,100,bmpLibrary[BMPL::BUTTON],ability_font,al_map_rgb(255,255,255),"HIGH SCORES",std::bind(changeToGui,std::ref(openGUI),guiLibrary[GUIL::HIGHSCORES])));
    guiLibrary[GUIL::MAIN_MENU]->add(new BUTTON(350,250,200,100,bmpLibrary[BMPL::BUTTON],ability_font,al_map_rgb(255,255,255),"CONFIG", std::bind(changeToGui,std::ref(openGUI),guiLibrary[GUIL::MMCONFIG])));
    guiLibrary[GUIL::MAIN_MENU]->add(new BUTTON(350,350,200,100,bmpLibrary[BMPL::BUTTON],ability_font,al_map_rgb(255,255,255),"EXIT", std::bind([&game](){game->endGame();})));//endGame, std::ref(loop))));

        ///Pause_Menu
    guiLibrary[GUIL::PAUSE_MENU]->add(new BUTTON(150,150,200,100,bmpLibrary[BMPL::BUTTON],ability_font,al_map_rgb(255,255,255),"RESUME",std::bind([&game](){game->startGame();})));//starGame,std::ref(openGUI),std::ref(isGUIopen),std::ref(isPaused))));
    guiLibrary[GUIL::PAUSE_MENU]->add(new BUTTON(350,250,200,100,bmpLibrary[BMPL::BUTTON],ability_font,al_map_rgb(255,255,255),"CONFIG",std::bind(changeToGui,std::ref(openGUI),guiLibrary[GUIL::CONFIG_MENU])));
    guiLibrary[GUIL::PAUSE_MENU]->add(new BUTTON(150,350,200,100,bmpLibrary[BMPL::BUTTON],ability_font,al_map_rgb(255,255,255),"MAIN MENU",std::bind(changeToGui,std::ref(openGUI),guiLibrary[GUIL::MAIN_MENU])));
    guiLibrary[GUIL::PAUSE_MENU]->add(new BUTTON(350,350,200,100,bmpLibrary[BMPL::BUTTON],ability_font,al_map_rgb(255,255,255),"SAVE/LOAD",std::bind(changeToGui,std::ref(openGUI),guiLibrary[GUIL::SAVELOAD])));
    guiLibrary[GUIL::PAUSE_MENU]->add(new BUTTON(350,150,200,100,bmpLibrary[BMPL::BUTTON],ability_font,al_map_rgb(255,255,255),"EXIT", std::bind([&game](){game->endGame();})));//endGame, std::ref(loop))));
    guiLibrary[GUIL::PAUSE_MENU]->add(new BUTTON(150,250,200,100,bmpLibrary[BMPL::BUTTON],ability_font,al_map_rgb(255,255,255),"RESTART",std::bind([&game](){game->restart();})));//restart,std::ref(openGUI),std::ref(isGUIopen),std::ref(isPaused),std::ref(isPlayerTurn),std::ref(hostiles),std::ref(per),std::ref(mundo),std::ref(state),std::ref(level),std::ref(killcount),std::ref(turn),true)));

        ///CONFIG_MENU
    guiLibrary[GUIL::CONFIG_MENU]->add(new BUTTON(410,348,200,100,bmpLibrary[BMPL::BUTTON],ability_font,al_map_rgb(255,255,255),"RETURN",std::bind(changeToGui,std::ref(openGUI),guiLibrary[GUIL::PAUSE_MENU])));
    guiLibrary[GUIL::CONFIG_MENU]->add(new SLIDEBAR(70,130,218,29,10,bmpLibrary[BMPL::CURSOR],bmpLibrary[BMPL::SLIDER],true,CONFIG::gain,ability_font,al_map_rgb(255,255,255),"Music"));
    guiLibrary[GUIL::CONFIG_MENU]->add(new SLIDEBAR(70,190,218,29,10,bmpLibrary[BMPL::CURSOR],bmpLibrary[BMPL::SLIDER],true,CONFIG::sfx_gain,ability_font,al_map_rgb(255,255,255),"SFX"));

        ///CREDITOS
    guiLibrary[GUIL::CREDITOS]->add(new BUTTON(413,352,200,100,bmpLibrary[BMPL::BUTTON],ability_font,al_map_rgb(255,255,255),"RETURN", std::bind(changeToGui,std::ref(openGUI), guiLibrary[GUIL::MAIN_MENU])));

        ///HIGHSCORES
    guiLibrary[GUIL::HIGHSCORES]->add(new BUTTON(413,352,200,100,bmpLibrary[BMPL::BUTTON],ability_font,al_map_rgb(255,255,255),"RETURN", std::bind(changeToGui,std::ref(openGUI), guiLibrary[GUIL::MAIN_MENU])));
    guiLibrary[GUIL::HIGHSCORES]->add(generate_hs_table(db, big_font));

        ///LOAD
    guiLibrary[GUIL::LOAD]->add(new BUTTON(413,352,200,100,bmpLibrary[BMPL::BUTTON],ability_font,al_map_rgb(255,255,255),"RETURN", std::bind(changeToGui,std::ref(openGUI), guiLibrary[GUIL::MAIN_MENU])));
    guiLibrary[GUIL::LOAD]->add(generate_load_table(db, big_font, game));

        ///SAVELOAD
    {
        guiLibrary[GUIL::SAVELOAD]->add(new BUTTON(413,352,200,100,bmpLibrary[BMPL::BUTTON],ability_font,al_map_rgb(255,255,255),"RETURN", std::bind(changeToGui,std::ref(openGUI), guiLibrary[GUIL::PAUSE_MENU])));
        auto but = new TEXTBOX(30,350,180,70,big_font,al_map_rgb(50,50,50),al_map_rgb(255,255,255),"Save name");
        guiLibrary[GUIL::SAVELOAD]->add(but);
        guiLibrary[GUIL::SAVELOAD]->add(generate_saveload_table(db, big_font, game, guiLibrary[GUIL::PAUSE_MENU]));
    }


        ///CONFIG_MENU
    guiLibrary[GUIL::MMCONFIG]->add(new BUTTON(410,348,200,100,bmpLibrary[BMPL::BUTTON],ability_font,al_map_rgb(255,255,255),"RETURN",std::bind(changeToGui,std::ref(openGUI),guiLibrary[GUIL::MAIN_MENU])));
    guiLibrary[GUIL::MMCONFIG]->add(new SLIDEBAR(70,130,218,29,10,bmpLibrary[BMPL::CURSOR],bmpLibrary[BMPL::SLIDER],true,CONFIG::gain,ability_font,al_map_rgb(255,255,255),"Music"));
    guiLibrary[GUIL::MMCONFIG]->add(new SLIDEBAR(70,190,218,29,10,bmpLibrary[BMPL::CURSOR],bmpLibrary[BMPL::SLIDER],true,CONFIG::sfx_gain,ability_font,al_map_rgb(255,255,255),"SFX"));

        ///ENDGAME
    {
        guiLibrary[GUIL::ENDGAME]->add(new BUTTON(413,352,200,100,bmpLibrary[BMPL::BUTTON],ability_font,al_map_rgb(255,255,255),"RETURN", std::bind(changeToGui,std::ref(openGUI), guiLibrary[GUIL::MAIN_MENU])));
        auto but = new TEXTBOX(30,350,180,70,big_font,al_map_rgb(50,50,50),al_map_rgb(255,255,255),"Insert name");
        guiLibrary[GUIL::ENDGAME]->add(but);
        guiLibrary[GUIL::ENDGAME]->add(new BUTTON(230,350,180,100,bmpLibrary[BMPL::BUTTON],ability_font,al_map_rgb(255,255,255),"SAVE SCORE", std::bind(savescore,std::ref(openGUI), guiLibrary[GUIL::MAIN_MENU],db,std::ref(but->text),std::ref(game)))); //std::bind(changeToGui,std::ref(openGUI), guiLibrary[GUIL::MAIN_MENU])));
        guiLibrary[GUIL::ENDGAME]->add(generate_hs_table(db, big_font));
    }
}



struct General {
    General() = default;
    int init();
    ~General();

    void mainloop();


    ALLEGRO_DISPLAY* display = nullptr;
    ALLEGRO_TIMER* screen_update_timer = nullptr;
    ALLEGRO_TIMER* tu_timer = nullptr;
    ALLEGRO_EVENT_QUEUE* event_queue = nullptr;
    ALLEGRO_BITMAP* icon_ankh;
    ALLEGRO_BITMAP* backg;
    ALLEGRO_AUDIO_STREAM* audio_stream = nullptr;
    bool isGUIopen = true;
    GUI* openGUI = nullptr;

    ALLEGRO_FONT* ability_font;
    ALLEGRO_FONT* big_font;
    ALLEGRO_FONT* target_font;

    sqlite3 *db;

    std::unique_ptr<Game> game;
};




/**==========================================================================**/
int main() {
    General tudo{};
    int ret = tudo.init();
    if(ret){
        return ret;
    }
    tudo.mainloop();
    return 0;
}

int General::init(){
    ///Inicializa Allegro5
    if (!al_init()) {
        Logger::getI() << "Error could not initialize Allegro5";
        return -1;
    }
    Logger::getI() << "Allegro 5 initialized";
    ///Inicializa Addons
    if (!al_init_image_addon()) {
        Logger::getI() << "Error could not initialize Allegro5 Image Addon";
        return -1;
    }
    Logger::getI() << "Allegro image Addon Initialized";
    al_init_font_addon();
    if (!al_init_ttf_addon()) {
        Logger::getI() << "Error could not initialize Allegro5 TTF Addon";
        return -1;
    }
    Logger::getI() << "Allegro TTF Addon Initialized";
    if (!al_init_primitives_addon()) {
        Logger::getI() << "Error could not initialize Allegro5 primitives Addon";
        return -1;
    }
    Logger::getI() << "Allegro primitives Addon Initialized";
    if (!al_install_audio()) {
        Logger::getI() << "Error could not initialize Allegro5 Audio Addon";
        return -1;
    }
    Logger::getI() << "Allegro Audio Addon Initialized";
    if (!al_init_acodec_addon()) {
        Logger::getI() << "Error could not initialize Allegro5 Audio Codecs";
        return -1;
    }
    Logger::getI() << "Allegro Audio Codecs Initialized";
    ///Instala Hardwares
    if (!al_install_keyboard()) {
        Logger::getI() << "Could not detect a keyboard conected to the PC";
    }
    Logger::getI() << "Keyboard initialized";
    if (!al_install_mouse()) {
        Logger::getI() << "Could not detect a mouse connected to the PC";
    }
    Logger::getI() << "Mouse Initialized";
    ///Le e/ou salva arquivo de configuracoes
    if (!CONFIG::openFile(CONFIG_F_PATH)) {
        ///Cria arquivo de configuracao padrao
	CONFIG::creatConf();
        Logger::getI() << "Config file created";
    } else {
        ///Le arquivo de configuracao
        CONFIG::loadConf();
        Logger::getI() << "Config file loaded";
    }
    CONFIG::saveFile(CONFIG_F_PATH);
    CONFIG::destrConf();
    ///Inicializa Variaveis
    display = al_create_display(CONFIG::screen_w, CONFIG::screen_h);
    if (!display) {
        Logger::getI() << "failed to create display!";
        return -1;
    }
    al_set_window_title(display, ANKH_APP_NAME);
    icon_ankh = al_load_bitmap(ANKH_APP_ICON);
    if (icon_ankh) {
        al_set_display_icon(display, icon_ankh);
        al_destroy_bitmap(icon_ankh);
    }
    Logger::getI() << "Screen created";
    audio_stream = al_load_audio_stream("resources/audio/soundtrack.wav",4,1024);
    if(!audio_stream){
        al_destroy_display(display);
        Logger::getI()<<"Error could not load soundtrack";
        return(-1);
    }
    if(!al_reserve_samples(1)){
        al_destroy_audio_stream(audio_stream);
        al_destroy_display(display);
        Logger::getI()<<"Error could not reserve audio sample";
        return(-1);
    }
    screen_update_timer = al_create_timer(1.0 / CONFIG::fps);
    if (!screen_update_timer) {
        Logger::getI() << "Failed to create timer!";
        al_destroy_audio_stream(audio_stream);
        al_destroy_display(display);
        return -1;
    }
    tu_timer = al_create_timer(1.0/100.0);
    if(!tu_timer){
        Logger::getI() << "Failed to create timer!";
        al_destroy_audio_stream(audio_stream);
        al_destroy_display(display);
        al_destroy_timer(screen_update_timer);
        return -1;
    }
    event_queue = al_create_event_queue(); //fila de eventos
    if (!event_queue) {
        Logger::getI() << "Failed to create event_queue!";
        al_destroy_audio_stream(audio_stream);
        al_destroy_display(display);
        al_destroy_timer(screen_update_timer);
        al_destroy_timer(tu_timer);
        return -1;
    }
    backg = al_load_bitmap("resources/back.png");
    ///Registra fontes de Evento
    al_register_event_source(event_queue, al_get_display_event_source(display));
    al_register_event_source(event_queue, al_get_timer_event_source(screen_update_timer));
    al_register_event_source(event_queue, al_get_timer_event_source(tu_timer));
    al_register_event_source(event_queue, al_get_keyboard_event_source());
    al_register_event_source(event_queue, al_get_mouse_event_source());
    al_set_target_bitmap(al_get_backbuffer(display));
    al_clear_to_color(al_map_rgb(0, 0, 0));
    al_flip_display();

    ///BITMAP_LIB
    bmpLibrary.add(al_load_bitmap("resources/gui/main_menu_background.png"));
    bmpLibrary.add(al_load_bitmap("resources/gui/button.png"));
    bmpLibrary.add(al_load_bitmap("resources/gui/menu_background.png"));
    bmpLibrary.add(al_load_bitmap("resources/gui/cursor.png"));
    bmpLibrary.add(al_load_bitmap("resources/gui/slider_background.png"));
    bmpLibrary.add(al_load_bitmap("resources/gui/creditos_background.png"));

    ability_font = al_load_font("resources/ttf/Papyrus.ttf", 16, 0);
    big_font     = al_load_font("resources/ttf/Papyrus.ttf", 40, 0);
    target_font  = al_load_font("resources/ttf/Papyrus.ttf", 25, 0);

    int rc = sqlite3_open("highscores.sqlite", &db);
    if( rc ){
        Logger::getI() << "Can't open database:";
        Logger::getI() << sqlite3_errmsg(db);
        sqlite3_close(db);
        return -1;
    }

    game = std::unique_ptr<Game>{new Game{
        isGUIopen,
        openGUI,
        screen_update_timer, //receber?
        tu_timer, //receber?

        ability_font,// receber
        target_font,// receber
        big_font,// receber

        event_queue, // receber

        audio_stream,

        display,

        &guiLibrary // receber
        }};

    initGuiLib(guiLibrary, ability_font, big_font, game, db, openGUI);

    openGUI = guiLibrary[GUIL::MAIN_MENU];
    ///Inicializa Timers
    al_start_timer(screen_update_timer);
    al_start_timer(tu_timer);
    ///Laco principal
    Logger::getI() << "Start Ankh";

    ///SFXs
    SFX_PLAYER::load();
    SFX_PLAYER::play(SFXs::NOP);
    ///MUSICA DE FUNDO
    al_attach_audio_stream_to_mixer(audio_stream,al_get_default_mixer());
    al_set_audio_stream_playmode(audio_stream,ALLEGRO_PLAYMODE_LOOP);
    al_set_audio_stream_gain(audio_stream,CONFIG::gain);
    al_set_audio_stream_playing(audio_stream,true);
    return 0;
}

void General::mainloop(){
    game->mainloop();
    // TODO: delete game.
}

General::~General(){
    al_destroy_font(ability_font);
    al_destroy_font(target_font);
    al_destroy_font(big_font);
    al_set_audio_stream_playing(audio_stream,false);
    al_destroy_audio_stream(audio_stream);
    SFX_PLAYER::free();
    CONFIG::creatConf();
    CONFIG::saveFile(CONFIG_F_PATH);
    CONFIG::destrConf();
    guiLibrary.free();
    bmpLibrary.free();
    sqlite3_close(db);
    ///Desistala Hardware
    al_uninstall_keyboard();
    al_uninstall_mouse();
    ///Destrutores
    al_stop_timer(screen_update_timer);
    Logger::getI() << "Ankh end execution...";
    if (backg) {
        al_destroy_bitmap(backg);
    }
    al_shutdown_image_addon();
    al_shutdown_ttf_addon();
    al_shutdown_font_addon();
    al_shutdown_primitives_addon();
    al_destroy_timer(screen_update_timer);
    al_destroy_timer(tu_timer);
    al_destroy_display(display);
    al_destroy_event_queue(event_queue);
}

/**==========================================================================**/
