#include "../include/Buff.hpp"


bool Buff::end_turn(int , World& world) {
    effects.activate(world);
    return true;
}

bool TemporaryBuff::end_turn(int turn, World& world) {
    if (--turns_remaining) {
        return Buff::end_turn(turn, world);
    } else {
        Buff::end_turn(turn, world);
        return false;
    }
}

void Buff::storegame(sqlite3* db, std::string& savename){
  //todo
}
void Buff::loadgame(sqlite3* db, std::string& savename){
  //todo
}
