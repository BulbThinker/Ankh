#include <chrono>

#include "../include/util.hpp"
#include "../include/World.hpp"
#include "../include/Stat.hpp"

std::default_random_engine rd(std::chrono::system_clock::now().time_since_epoch().count());
std::mt19937 gen = std::mt19937(rd());

void draw_thing_text(ALLEGRO_FONT* font, int posX, int posY, char const * const thing, ALLEGRO_COLOR color, int flags){
  al_draw_text(font, color, posX, posY, flags, thing);
}

void draw_thing_text(ALLEGRO_FONT* font, int posX, int posY, const std::string &thing, ALLEGRO_COLOR color, int flags){
  al_draw_text(font, color, posX, posY, flags, thing.c_str());
}

void draw_thing_text(ALLEGRO_FONT* font, int posX, int posY, const std::stringstream &thing, ALLEGRO_COLOR color, int flags){
  al_draw_text(font, color, posX, posY, flags, thing.str().c_str());
}

ALLEGRO_COLOR merge_colors(const std::vector<ALLEGRO_COLOR>& colors){
  float r = 1.0;
  float g = 1.0;
  float b = 1.0;
  float a = 1.0;

  float r2;
  float g2;
  float b2;
  float a2;

  for(auto c = colors.begin(); c != colors.end(); c++){
    al_unmap_rgba_f(*c, &r2, &g2, &b2, &a2);
    r *= r2;
    g *= g2;
    b *= b2;
    a *= a2;
  }

  return al_map_rgba_f(r, g, b, a);
}
