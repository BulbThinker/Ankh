#include "../include/World.hpp"
#include "../include/util.hpp"

World::World(char const * name, int width, int height, coord screen, int tileSize) {
    char fname[50];
    this->width = width;
    this->height = height;
    screenPos.x = screen.x;
    screenPos.y = screen.y;
    this->tileSize = tileSize;
    nextId = 0;
    sprintf(fname, "resources/sprites/%s", name);
    background = al_load_bitmap(fname);
}
World::~World() {
    al_destroy_bitmap(background);
}
map<int, Entity*> *World::getEntities() {
    return &entities;
}
int *World::getNumEntities() {
    return &nextId;
}
void World::setScreenPos(coord c) {
    screenPos.x = c.x;
    screenPos.y = c.y;
}
coord World::getScreenPos() {
    return screenPos;
}
void World::insertEntity(Entity *ent) {
    ent->setId(nextId);
    entities[nextId++] = ent;
}
bool World::sortFunction(Entity *i, Entity *j) {
    return(i->getY() < j->getY());
    /*if (i->getY() + i->getHeight() == i->getY() + i->getHeight()) {
        return i->getX() + i->getWidth() < i->getX() + i->getWidth();
    } else {
        return i->getY() + i->getHeight() < i->getY() + i->getHeight();
    }*/
}
void World::resetEntities() {
    map<int, Entity*>::iterator it;
    for (it = entities.begin(); it != entities.end(); it++) {
        it->second->reset();
    }
}
void World::selectVisibleEntities(coord origin, int width, int height) {
    map<int, Entity*>::iterator it;
    visibleEntities.clear();
    for (it = entities.begin(); it != entities.end(); it++) {
        if (it->second->getX() + it->second->getWidth() >= origin.x
            && it->second->getX() <= origin.x + width
            && it->second->getY() + it->second->getHeight() >= origin.y
            && it->second->getY() <= origin.y + height) {
            visibleEntities.push_back(it->second);
        }
    }
}
void World::drawEntities(coord origin) {
    vector<Entity*>::iterator it;
    sort(visibleEntities.begin(), visibleEntities.end(), sortFunction);
    for (it = visibleEntities.begin(); it != visibleEntities.end(); it++) {
        if ((*it)->isAlive()) {
            (*it)->drawSprite(origin);
        }
    }
}
void World::handleColision() {
    vector<Entity*>::iterator it1, it2;
    for (it1 = visibleEntities.begin(); it1 != visibleEntities.end(); it1++) {
        for (it2 = it1 + 1; it2 != visibleEntities.end(); it2++) {
            if ((*it1)->isAlive() && (*it2)->isAlive()
                && ((*it1)->collide(*(*it2), coord(0, 1))
                    || (*it1)->collide(*(*it2), coord(1, 0))
                    || (*it1)->collide(*(*it2), coord(0, -1))
                    || (*it1)->collide(*(*it2), coord(-1, 0)))) {
                //collision logic here
            }
        }
    }
}
bool World::inBorder(Entity &ent, Direction dir) {
    if (dir == _up && ent.getY() <= 0) {
        return true;
    } else if (dir == _down && ent.getY() >= height * tileSize - 1) {
        return true;
    } else if (dir == _left && ent.getX() <= 0) {
        return true;
    } else if (dir == _right && ent.getX() >= width * tileSize - 1) {
        return true;
    }
    return false;
}
bool World::inCell(Entity &ent, coord pos, Direction dir) {
    if (dir == _up && ent.getY() == pos.y * tileSize
        && ent.getX() >= pos.x * tileSize
        && ent.getX() <= (pos.x + 1) * tileSize) {
        return true;
    } else if (dir == _down && ent.getY() + ent.getHeight() == (pos.y + 1) * tileSize
               && ent.getX() >= pos.x * tileSize
               && ent.getX() <= (pos.x + 1) * tileSize) {
        return true;
    } else if (dir == _left && ent.getX() == pos.x * tileSize
               && ent.getY() >= pos.y * tileSize
               && ent.getY() <= (pos.y + 1) * tileSize) {
        return true;
    } else if(dir == _right && ent.getX() + ent.getWidth() == (pos.x + 1) * tileSize
               && ent.getY() >= pos.y * tileSize
               && ent.getY() <= (pos.y + 1) * tileSize) {
        return true;
    }
    return false;
}
int World::getWidth() {
    return width;
}
int World::getHeight() {
    return height;
}
Coord World::random_position(){
  std::uniform_int_distribution<> disX(0, getWidth()-1);
  std::uniform_int_distribution<> disY(0, getHeight()-1);

  return Coord{disX(gen)*TILE_SIZE, disY(gen)*TILE_SIZE};
}
int World::getRealWidth() {
    return width * tileSize;
}
int World::getRealHeight() {
    return height * tileSize;
}
int World::getTileSize() const {
    return tileSize;
}
void World::draw(coord origin, int viewWidth, int viewHeight) {
    al_draw_bitmap(background, -origin.x, -origin.y, 0);
}
